rust-criterion (0.5.1-10) unstable; urgency=medium

  * update git-buildpackage config: fix usage comments
  * update watch file: mangle upstream version

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 09 Feb 2025 09:22:44 +0100

rust-criterion (0.5.1-9) unstable; urgency=medium

  * fix tighten (build--dependency for crate smol
  * declare rust-related build-dependencies unconditionally,
    i.e. drop broken nocheck annotations
  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 06 Feb 2025 14:12:56 +0100

rust-criterion (0.5.1-8) unstable; urgency=medium

  * tighten build-dependency for crate itertools

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 04 Oct 2024 16:55:47 +0200

rust-criterion (0.5.1-7) unstable; urgency=medium

  * stop mention dh-cargo in long description
  * add patches cherry-picked upstream,
    superseding patches 1001_*
  * add patch 2003
    to avoid build failure due to "unexpected cfg" lints;
    closes: bug#1081878, thanks to Peter Michael Green
  * add patch 2001
    to accept newer branches of crate itertools;
    relax (build-)dependencies for crate itertools;
    closes: bug#1082548, thanks to Peter Michael Green

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 22 Sep 2024 09:42:41 +0200

rust-criterion (0.5.1-6) unstable; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * reduce autopkgtest to check single-feature tests only on amd64

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 30 Jul 2024 17:43:20 +0200

rust-criterion (0.5.1-5) unstable; urgency=medium

  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 12 Jul 2024 12:44:08 +0200

rust-criterion (0.5.1-4) unstable; urgency=medium

  * update dh-cargo fork
  * update copyright info: update coverage
  * declare compliance with Debian Policy 4.7.0
  * relax to build-depend unversioned
    when version is satisfied in Debian stable
  * fix build-depend (not only depend) on packages
    for crates async-std smol tokio
  * update DEP-3 headers
  * add patch 1001 to accept newer branch of crate smol;
    relax (build-)dependency for crate smol

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 24 Jun 2024 18:36:50 +0200

rust-criterion (0.5.1-3) unstable; urgency=medium

  * (build-)depend on package for crate ciborium (not serde_cbor)

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 18 Aug 2023 16:37:26 +0200

rust-criterion (0.5.1-2) unstable; urgency=medium

  * update dh-cargo fork;
    closes: bug#1048330, thanks to Lucas Nussbaum
  * update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 13:12:06 +0200

rust-criterion (0.5.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)
    + add test case for BytesDecimal;
      closes: bug#1031306, thanks to Adrian Bunk

  [ Jonas Smedegaard ]
  * update DEP-3 headers for patch 2001
  * mention patched-away feature real_blackbox as TODO
  * unfuzz patch 2001
  * (build-)depend on package for crates
    is-terminal once_cell (not atty lazy_static);
    tighten (build-)dependencies for crates clap tempfile
  * bump version for provided virtual packages and autopkgtest hints
  * add patch 1001
    to relax dependency to match packaged crate tempfile 3.6.0

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 15 Jul 2023 19:12:50 +0200

rust-criterion (0.4.0-2) unstable; urgency=medium

  * adjust package relations:
    + (build-)epend on package for crate anes
    + depend (not only build-depend) for crate csv
    + tighten dependency for crate lazy-static
    + relax (build-)dependency for crate clap
    + relax build-dependency for crate oorandom
    + drop dependency for older crate criterion-plot
    + drop autopkgtest-dependency for crate futures

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Feb 2023 11:22:59 +0100

rust-criterion (0.4.0-1) experimental; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * bump version for provided virtual packages and autopkgtest hints
  * tighten autopkgtests
  * drop patch cherry-picked upstream now applied
  * drop patch 1001 adopted upstream
  * drop patch 1002 obsoleted by upstream changes
  * add patch 2001 to avoid crate ciborium
  * unfuzz patch 2002
  * tighten (build-)dependencies for crates clap num-traits regex
  * provide virtual package for (not build-depend on)i
    embedded crate criterion-plot

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 03 Feb 2023 10:19:33 +0100

rust-criterion (0.3.6-4) unstable; urgency=medium

  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * stop superfluously provide virtual unversioned feature packages
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 15:46:50 +0100

rust-criterion (0.3.6-3) unstable; urgency=medium

  * drop patch 2001, obsoleted by Debian package updates;
    (build-)depend on package for crate plotters;
    suggest (not depend on) gnuplot-nox
  * stop superfluously provide virtual packages
    for zero- and full-versioned features
  * merge virtual feature package into main binary package
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 07 Dec 2022 10:27:42 +0100

rust-criterion (0.3.6-2) unstable; urgency=medium

  [ Michael Green ]
  * update inconsistent cast dependencies to a consistent 0.3:
    the cast dependency in the main Cargo.toml was at 0.3
    while the Debian Dependency
    and the cast dependency in the plot subcrate
    were at 0.2.

  [ Jonas Smedegaard ]
  * extend upstream-cherry-picked patch
    to also cover crates num-complex rand;
    update DEP-3 patch headers
  * fix build-depend on packages for crates itertools-num num-complex;
    add TODO note on providing crate criterion-plot in binary package
    (i.e. replace needlessly separate source package),
    thanks to Peter Michael Green
  * add TODO note on possible need for improved cleanup,
    thanks to Peter Michael Green

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 23 Nov 2022 13:58:07 +0100

rust-criterion (0.3.6-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * gix depend on (not recommend) gnuplot;
    add TODO and README.Debian
    documenting deviation from upstream
    of always using gnuplot (not fall back to crate plotters)
  * add patch 2002
    to avoid feature real_blackbox requiring nightly functions
  * stop build-depend on libstd-rust-dev rustc, not used explicitly
  * bump project version in virtual packages and autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Nov 2022 08:51:45 +0100

rust-criterion (0.3.5-3) unstable; urgency=medium

  * drop virtual packages and autopkgtest
    for nightly-only feature real_blackbox
  * extend patch 2001 to silence a cosmetic warning
  * add patch 1002 to modernize macro syntax

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 26 Jun 2022 17:30:50 +0200

rust-criterion (0.3.5-2) unstable; urgency=medium

  * fix stop depend on crate plotters, avoided through patch
  * update dh-cargo fork
  * drop metapackages for bogus features,
    and drop corresponding autopkgtests
  * tighten autopkgtest dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 24 Jun 2022 19:08:55 +0200

rust-criterion (0.3.5-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1013346

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 22 Jun 2022 16:27:10 +0200
